--mysql -u root
--List databases
SHOW DATABASES;
-- create database
CREATE DATABASE music_db;
-- delete database
DROP DATABASE music_db;
-- Select database
USE music_db;
-- create a table
-- format : [column_name], [data_type], [other_options]
CREATE TABLE users(
  id INT NOT NULL AUTO_INCREMENT,
  username VARCHAR(50) NOT NULL,
  password VARCHAR(50) NOT NULL,
  full_name VARCHAR(50) NOT NULL,
  contact_number INT NOT NULL,
  email VARCHAR(50),
  address VARCHAR(50),
  PRIMARY KEY (id)
);

CREATE TABLE artists(
  id INT NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE albums(
  id INT NOT NULL AUTO_INCREMENT,
  album_title VARCHAR(50) NOT NULL,
  date_released DATE NOT NULL,
  artist_id INT NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT fk_albums_artist_id
    FOREIGN KEY (artist_id) REFERENCES artists(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT
);

CREATE TABLE songs(
  id INT NOT NULL AUTO_INCREMENT,
  song_name VARCHAR(50) NOT NULL,
  length TIME NOT NULL,
  genre VARCHAR(50) NOT NULL,
  album_id INT NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT fk_songs_albums_id
    FOREIGN KEY (album_id) REFERENCES albums(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT
);

CREATE TABLE playlists(
  id INT NOT NULL AUTO_INCREMENT,
  user_id INT NOT NULL,
  datetime_created DATETIME NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT fk_playlists_user_id
    FOREIGN KEY (user_id) REFERENCES users(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT
);

CREATE TABLE playlists_songs(
  id INT NOT NULL AUTO_INCREMENT,
  playlist_id INT NOT NULL,
  song_id INT NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT fk_playlists_songs_playlist_id
    FOREIGN KEY (playlist_id) REFERENCES playlists(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT,
  CONSTRAINT fk_playlists_songs_song_id
    FOREIGN KEY (song_id) REFERENCES songs(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT
);

CREATE TABLE reviews(
  id INT NOT NULL AUTO_INCREMENT,
  review VARCHAR(300) NOT NULL,
  datetime_created DATETIME NOT NULL,
  PRIMARY KEY (id)
);

-- DISPLAY SPECIFIC TABLE
SHOW TABLES;
-- remove specific table
DROP TABLE reviews;
-- show details
DESCRIBE reviews;
-- alter a table
-- drop column/field from table
ALTER TABLE reviews DROP COLUMN review;
-- add a new field in a table
ALTER TABLE reviews ADD comments VARCHAR(300) NOT NULL;